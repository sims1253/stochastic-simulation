library(boot)
v.shape = 2
v.scale = 2

# 2.1
non_parametric_bootstrap = function(x, N){
  sample_means = rep(0, N)
  for (i in 1:N){
    loop_sample = sample(x, size=length(x), replace=TRUE)
    sample_means[i] = mean(loop_sample)
  }
  return(sample_means)
}

sample_means = non_parametric_bootstrap(rgamma(1000, shape=v.shape, scale=v.scale), 1000)
hist(sample_means)
selfmade_bias = mean(sample_means) - v.shape * v.scale
selfmade_var = var(sample_means)


# 2.2
samples_for_boot = rgamma(1000, shape=v.shape, scale=v.scale)

bootstrap_mean = function(data, indices){
  return(mean(data[indices[1:100]]))
}

boot_result = boot(data=samples_for_boot, statistic=bootstrap_mean, R=1000)
boot_bias = mean(boot_result$t) - v.shape * v.scale
boot_var = var(boot_result$t)[1]
boot_mean = mean(boot_result$t)


# 2.3
ordered = sort(boot_result$t)
basic = c(2 * boot_mean - ordered[1001*(1-0.025)], 2 * boot_mean - ordered[1001*(0.025)])
normal = c(boot_mean -(qnorm(1-0.025))*sd(boot_result$t), boot_mean -(qnorm(0.025))*sd(boot_result$t))
percentile = c(ordered[1001 * 0.025], ordered[1001 * (1-0.025)])
boot.ci(boot_result)


# 2.4
list_of_hit_ratios = rep(0,10)
true_mean = v.shape*v.scale
for (n in seq(10, 100, by=10)){
  hits = 0
  for (i in 1:1000){
    bootstrap = sort(non_parametric_bootstrap(rgamma(n, shape=v.shape, scale=v.scale), 500))
    percentile = c(bootstrap[(length(bootstrap)+1) * 0.025], bootstrap[(length(bootstrap)+1) * (1-0.025)])
    if (percentile[1] <= true_mean && percentile[2] >= true_mean){
      hits = hits + 1
    }
  }
  list_of_hit_ratios[n/10] = hits/1000
}

plot(seq(10,100,10), list_of_hit_ratios)
abline(a=0.95, b=0, lty=2)
