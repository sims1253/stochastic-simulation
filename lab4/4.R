library(boot)
v.shape = 2
v.scale = 2

# 4.1
non_parametric_bootstrap = function(x, N){
  sample_means = rep(0, N)
  for (i in 1:N){
    loop_sample = sample(x, size=length(x), replace=TRUE)
    sample_means[i] = var(loop_sample)
  }
  return(sample_means)
}

list_of_hit_ratios = rep(0,10)
true_var = v.shape * v.scale^2
for (n in seq(10, 100, by=10)){
  hits = 0
  for (i in 1:1000){
    bootstrap = sort(non_parametric_bootstrap(rgamma(n, shape=v.shape, scale=v.scale), 1000))
    percentile = c(bootstrap[ceiling((n+1) * 0.025)], bootstrap[floor((n+1) * (1-0.025))])
    if (percentile[1] <= true_var && percentile[2] >= true_var){
      hits = hits + 1
    }
  }
  list_of_hit_ratios[n/10] = hits/1000
}

plot(seq(10,100,10), list_of_hit_ratios)
abline(a=0.95, b=0, lty=2)


# 4.2
