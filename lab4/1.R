v.shape = 2
v.scale = 2
data = rgamma(100, shape=v.shape, scale=v.scale)
hist(data)

# Formulas taken from Wikipedia
theoretical_mean = v.shape * v.scale
theoretical_variance = v.shape * v.scale^2

# Not sure this is what they are asking for
# in the 2nd questions
sample_mean = mean(data)
sample_variance = var(data)
