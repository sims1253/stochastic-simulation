/* To compile the program in a terminal execute:
gcc -fsanitize=address -fno-omit-frame-pointer -g lab5.c -o gcc-app -lm
To run the program in a terminal execute: ./app
*/

#include <math.h> //for mathfunctions
#include <stdio.h>
#include <stdlib.h>
#include <time.h> // for timing

double * ordMCIntegration(int n)
{
	double* result = malloc(sizeof(double) * 2);   
	double S = 0;
	double error = 0;

	for(int i = 1; i <= n; i++)
	{
		double x = drand48();
		double y = 4 / (1 + pow(x, 2));
		S = S + y;
		error = error + pow(y,2);
	}

	S = S / n;
	error = sqrt(error/n - pow(S,2)) / sqrt(n);

	result[0] = S;
	result[1] = error;
	return result;
}

double * importanceSampling(int n)
{
	double* result = malloc(sizeof(double) * 2);
	double S = 0;
	double variance = 0;

	for(int i = 1; i < n; i++)
	{
		double x = drand48();
		double nominator = 4 / (1 + pow(x, 2));
		double denominator =  (4 - 2 * x) / 3;
		S = S + nominator / denominator;
		variance = variance + pow(nominator / denominator,2);
	}

	S = S / n;
	double sd = sqrt(variance/n - pow(S, 2));

	result[0] = S;
	result[1] = sd/sqrt(n);
	return result;
}

double * controlVariates(int n)
{
	int Ig = 3;
	double* result = malloc(sizeof(double) * 2);
	double S = 0;
	double variance = 0;

	for(int i = 1; i < n; i++)
	{
		double x = drand48();
		double f = 4 / (1 + pow(x, 2));
		double g =  4 - 2 * x;
		S = S + f - g;
		variance = variance + pow((f - g + Ig), 2);
	}

	S = Ig + S / n;
	double sd = sqrt(variance/n - pow(S, 2));

	result[0] = S;
	result[1] = sd / sqrt(n);
	return result;
}

double * antitheticVariates(int n)
{
	double* result = malloc(sizeof(double) * 2);
	double S = 0;
	double variance = 0;

	for(int i = 1; i < n; i++)
	{
		double x = drand48();
		double firstSum = 4 / (1 + pow(x, 2));
		double secondSum =  4 / (1 + pow(1-x, 2));;
		S = S + firstSum / 2 + secondSum / 2;
		variance = variance + pow((firstSum / 2 + secondSum / 2), 2);
	}

	S = S / n;
	double sd = sqrt(variance/n - pow(S, 2));

	result[0] = S;
	result[1] = sd / sqrt(n);
	return result;
}

double * stratifiedSampling(int n, int stratas)
{
	double* result = malloc(sizeof(double) * 2);
	double fraction = (double) 1 / stratas;
	int nj = n / stratas;
	double strataAvg = 0;
	double strataVar = 0;

	for(double i = 0; i < 1 ; i = i + fraction)
	{
		double subSum = 0;
		double varSum = 0;
		for(int j = 0; j < nj; j++)
		{
			double x = drand48() * fraction + i;
			double term = 4 / (1 + pow(x, 2));
			subSum = subSum + term;
			varSum = varSum + pow(term, 2);
		}
		strataVar = strataVar + (varSum / nj - pow(subSum / nj, 2)) / (nj - 1);
		strataAvg = strataAvg + subSum / nj;
	}
	double grandVar = strataVar / pow(stratas, 2);
	double grandAvg = strataAvg / stratas;

	result[0] = grandAvg;
	result[1] = sqrt(grandVar) / sqrt(n) ;
	return result;

}

int main()
{
	srand48(time(NULL)); //Make a new seed for the random number generator

	double result1[5];
	double result2[5];
	double result3[5];
	double result4[5];
	double result5[5];

	double error1[5];
	double error2[5];
	double error3[5];
	double error4[5];
	double error5[5];

	for(int i = 4; i<=8; i++)
	{
		double *r = ordMCIntegration(pow(10,i));
		result1[i-4] = r[0];
		error1[i-4] = r[1];
		free(r);

		r = importanceSampling(pow(10,i));
		result2[i-4] = r[0];
		error2[i-4] = r[1];
		free(r);

		r = controlVariates(pow(10,i));
		result3[i-4] = r[0];
		error3[i-4] = r[1];
		free(r);

		r = antitheticVariates(pow(10,i));
		result4[i-4] = r[0];
		error4[i-4] = r[1];
		free(r);

		r = stratifiedSampling(pow(10,i), 4);
		result5[i-4] = r[0];
		error5[i-4] = r[1];
		free(r);
	}
	printf("PI Values:\n");
	printf("normal \t\timportance \tcontrol \tantithetic \tstrata \n");
	for (int i = 0; i<=4; i++)
	{
		printf("%f \t", result1[i]);
		printf("%f \t", result2[i]);
		printf("%f \t", result3[i]);
		printf("%f \t", result4[i]);
		printf("%f", result5[i]);
		printf("\n");
	}

	printf("========================================================================\n");
	printf("Errors:\n");
	printf("normal \t\t importance \t control \t antithetic \t strata \n");
	for (int i = 0; i<=4; i++)
	{
		printf("%f \t", error1[i]);
		printf("%f \t", error2[i]);
		printf("%f \t", error3[i]);
		printf("%f \t", error4[i]);
		printf("%f", error5[i]);
		printf("\n");
	}
}

